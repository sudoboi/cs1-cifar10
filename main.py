# Warning: importing order must not change, as importing TensorFlow before NumPy 
# and other libraries is known to cause segmentation fault under some Linux systems
import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf


def take_samples(x, y, subset, classes):
    '''
    take_samples(x, y, subset, classes)
    
    x: image array to be filled
    
    y: label array to be filled
    
    subset: subset of the actual dataset to be loaded into the arrays provided
    0 - train
    1 - test
    
    classes: list of classes to use for sample selection

    returns: image array, label array
    '''
    loader = tf.keras.datasets.cifar10.load_data()
    cnt = 0
    new_classes = [x for x in range(len(classes))]
    for pair in zip(loader[subset][0], loader[subset][1]):
        if pair[1] in classes:
            pos = classes.index(pair[1])
            label = new_classes[pos]
            x[cnt] = pair[0]
            y[cnt] = label
            cnt += 1
    return x, y


def load_dataset(num_classes = 10):
    '''
    load_dataset(num_classes = 10)

    num_classes: number of classes to pick in the dataset

    returns: image array (train), label array (train), image array (test), label array (test)
    '''
    if num_classes == 2:
        classes = [0, 5]
        x_train = np.zeros((10000, 32, 32, 3), dtype=np.uint8)
        x_test = np.zeros((2000, 32, 32, 3), dtype=np.uint8)
        y_train = np.zeros((10000, 1), dtype=np.uint8)
        y_test = np.zeros((2000, 1), dtype=np.uint8)
        x_train, y_train = take_samples(x_train, y_train, 0, classes)
        x_test, y_test = take_samples(x_test, y_test, 1, classes)
    elif num_classes == 5:
        classes = [0, 2, 4, 6, 8]
        x_train = np.zeros((25000, 32, 32, 3), dtype=np.uint8)
        x_test = np.zeros((5000, 32, 32, 3), dtype=np.uint8)
        y_train = np.zeros((25000, 1), dtype=np.uint8)
        y_test = np.zeros((5000, 1), dtype=np.uint8)
        x_train, y_train = take_samples(x_train, y_train, 0, classes)
        x_test, y_test = take_samples(x_test, y_test, 1, classes)
    else:
        (x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
    return (x_train, y_train), (x_test, y_test)


def get_stats(array):
    value, frequency = np.unique(array, return_counts = True)
    for pair in zip(value, frequency):
        print(pair[0], ': ', pair[1])


def normalise_data(train, test):
    train = train / 255.0
    test = test / 255.0
    return train, test


def build_basic_network(output):
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Flatten(input_shape = (32, 32, 3)))
    if output == 1:
        loss = tf.keras.losses.BinaryCrossentropy(from_logits = True)
        act = 'sigmoid'
    else:
        loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits = True)
        act = 'softmax'
    model.add(tf.keras.layers.Dense(output, activation=act))

    model.compile(loss = loss,
            optimizer = 'adam',
            metrics = ['accuracy'])
    return model
  

def build_advanced_network(output, lr = 0.0001, dr = 0.9):
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Flatten(input_shape = (32, 32, 3)))
    
    model.add(tf.keras.layers.Dense(1024))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.ReLU())
    model.add(tf.keras.layers.Dropout(0.7))

    model.add(tf.keras.layers.Dense(512))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.ReLU())
    model.add(tf.keras.layers.Dropout(0.7))

    if output == 1:
        loss = tf.keras.losses.BinaryCrossentropy(from_logits = True)
        act = 'sigmoid'
    else:
        loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits = True)
        act = 'softmax'
    
    model.add(tf.keras.layers.Dense(output, activation = 'sigmoid'))
    model.compile(loss = loss,
            optimizer = tf.optimizers.Adam(lr, dr),
            metrics = ['accuracy'])
    return model


def param_search(dnn):
    # round() is used to make sure there are no extra digits present in the graphs or the rest of the data
    lr_range = [round(x, 4) for x in np.arange(0.0001, 0.001, 0.0003)]
    dr_range = [round(x, 1) for x in np.arange(0.1, 1.0, 0.4)]
    
    records = []

    for lr in lr_range:
        for dr in dr_range:
            print(lr, dr)
            model = dnn(10, lr = lr, dr = dr)
            (x_train, y_train), (x_test, y_test) = load_dataset(10)
            x_train, x_test = normalise_data(x_train, x_test)
            hist = model.fit(x_train, y_train, batch_size = 32, validation_data = (x_test, y_test), epochs = 50)
            loss, acc = model.evaluate(x_test, y_test)
            records.append(f'LR: {lr}; DR: {dr}; Loss: {loss}; Accuracy: {acc}')
            plt.plot(hist.history['val_accuracy'], label = f'LR: {lr}; DR: {dr}')
    plt.legend()
    plt.show()
    for item in records:
        print(item)


def visualise_training(history, title):
    plt.title(title + ' - Accuracy')
    plt.ylim([0,1])
    plt.plot(history.history['accuracy'], label = 'Train accuracy')
    plt.plot(history.history['val_accuracy'], label = 'Validation accuracy')
    plt.legend()
    plt.show()
    
    plt.title(title + ' - Loss')
    plt.plot(history.history['loss'], label = 'Train loss')
    plt.plot(history.history['val_loss'], label = 'Validation loss')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    
    print(20 * '#', ' Exercise 3 ', 20 * '#')
    (x_train, y_train), (x_test, y_test) = load_dataset(2)
    print('Training set frequencies: ')
    get_stats(y_train)
    print('Testing set frequencies: ')
    get_stats(y_test)
     
    print(20 * '#', ' Exercise 4 ', 20 * '#')
    x_train, x_test = normalise_data(x_train, x_test)
    model = build_basic_network(1)
    hist = model.fit(x_train, y_train, validation_data = (x_test, y_test), epochs = 50)

    print(20 * '#', ' Exercise 5 ', 20 * '#')
    loss, acc = model.evaluate(x_test, y_test)
    print('Test loss: ', loss)
    print('Test accuracy: ', acc)
    
    print(20 * '#', ' Exercise 6.1 ', 20 * '#')
    (x_train, y_train), (x_test, y_test) = load_dataset(5)
    x_train, x_test = normalise_data(x_train, x_test)
    model = build_basic_network(5)
    hist = model.fit(x_train, y_train, epochs = 50)
    loss, acc = model.evaluate(x_test, y_test)
    print('Test loss: ', loss)
    print('Test accuracy: ', acc)

    print(20 * '#', ' Exercise 6.2 ', 20 * '#')
    (x_train, y_train), (x_test, y_test) = load_dataset(10)
    x_train, x_test = normalise_data(x_train, x_test)
    model = build_basic_network(10)
    hist = model.fit(x_train, y_train, epochs = 50)
    loss, acc = model.evaluate(x_test, y_test)
    print('Test loss: ', loss)
    print('Test accuracy: ', acc)
        
    print(20 * '#', ' Exercise 7.1 ', 20 * '#')
    (x_train, y_train), (x_test, y_test) = load_dataset(2)
    x_train, x_test = normalise_data(x_train, x_test)
    model = build_advanced_network(1, 0.001, 0.5)
    hist = model.fit(x_train, y_train, validation_data = (x_test, y_test), epochs = 50)
    visualise_training(hist, 'Binary, BatchNorm, Dropout 0.7')
    loss, acc = model.evaluate(x_test, y_test)
    print('Test loss: ', loss)
    print('Test accuracy: ', acc)
    
    print(20 * '#', ' Exercise 7.2 ', 20 * '#')
    (x_train, y_train), (x_test, y_test) = load_dataset(5)
    x_train, x_test = normalise_data(x_train, x_test)
    #model = build_advanced_network(10, 0.01, 0.1)
    model = build_advanced_network(5, 0.003, 0.8)
    hist = model.fit(x_train, y_train, validation_data = (x_test, y_test), epochs = 50)
    visualise_training(hist, 'Multiclass, BatchNorm, Dropout 0.7')
    loss, acc = model.evaluate(x_test, y_test)
    print('Test loss: ', loss)
    print('Test accuracy: ', acc)

    print(20 * '#', ' Exercise 7.3 ', 20 * '#')
    (x_train, y_train), (x_test, y_test) = load_dataset(10)
    x_train, x_test = normalise_data(x_train, x_test)
    #model = build_advanced_network(10, 0.01, 0.1)
    model = build_advanced_network(10, 0.003, 0.8)
    hist = model.fit(x_train, y_train, validation_data = (x_test, y_test), epochs = 50)
    visualise_training(hist, 'Multiclass, BatchNorm, Dropout 0.7')
    loss, acc = model.evaluate(x_test, y_test)
    print('Test loss: ', loss)
    print('Test accuracy: ', acc)
    
    #param_search(build_advanced_network)
